﻿//This template is used to plug in user object values and display in the list
function prepareUserEntry(user) {
    var userHtml =
        "<div>" +
        "<h4>" + user.id + " - " + user.username + ": " + user.emailAddress + "</h4>" +
        user.lastName + ", " + user.firstName +
        "<button title=\"Delete this user\" onclick=\"deleteUser(" + user.id + ")\" id=\"deleteButton\" class=\"btn btn-sm btn-danger pull-right pull-up\">X</button>" +
        "</div>" +
        "<hr class=\"user-separator\" />";

    return userHtml;
}

//Perform a GET request to the "users" endpoint to get the list of users to show in the list
function getUsers() {
    $.getJSON("users", function (users) {
        if (users.length > 0) {
            users.forEach(function (user) {
                //Add the user to the list
                $("#registeredUsers").append(prepareUserEntry(user));
            });
        } else {
            $("#registeredUsers").append("<h4>No users to display :(</h4>");
        }
    });
}

//Post a new user to the "user/add" endpoint to be added to the "database", reload when done
function addUser(user) {
    $.post("user/new", user).done(function () {
        location.reload();
    });;
}

//Delete a user from the "database", reload when done
function deleteUser(id) {
    $.post("user/delete", { userId: id }).done(function () {
        location.reload();
    });
}

//Check whether our form validates
function checkTrueness(formState) {
    for (var obj in formState)
        if (!formState[obj]) return false;
    return true;
}

//Object to hold the validation state of the registration form
var formState = {
    firstName : false,
    lastName : false,
    username : false,
    passwordMatch : false,
};


// Document is ready
$(function () {
    //Let's grab the existing users (the test users) when the page loads
    getUsers();

    $("#deleteButton").click(function (e) {
        console.log($(this).closest('div').id);
    });

    //The following is the form validation logic
    //For firstName, we want to alert the user if they have >10 chars, or an empty field
    function checkFirstName(val, e) {
        var len = val.length;
        if (len > 10 || len <= 0) { //Incorrect field
            $("#firstNameErr").removeClass("text-hide");
            $("#firstNameGroup").addClass("has-error");
            formState.firstName = false;
            $("#submitUser").prop("disabled", true);
        } else {
            $("#firstNameErr").addClass("text-hide");
            $("#firstNameGroup").removeClass("has-error");
            formState.firstName = true;

            //Validate the whole form
            if (checkTrueness(formState))
                $("#submitUser").removeAttr("disabled");
        }
    }
    //We want to check the validity of the form any time the date in the fields changes
    $("#firstName").keyup(function (e) {
        checkFirstName($(this).val(), e);
    }).bind("paste", function (e) { //When pasting from context menu, you need a small delay to capture the text
        setTimeout(function () {
            checkFirstName($("#firstName").val(), e);
        }, 10);
    });

    //For lastName, we want to alert the user if they have >15 chars, or an empty field
    function checkLastName(val, e) {
        var len = val.length;
        if (len > 15 || len <= 0) { //Incorrect field
            $("#lastNameErr").removeClass("text-hide");
            $("#lastNameGroup").addClass("has-error");
            formState.lastName = false;
            $("#submitUser").prop("disabled", true);
        } else {
            $("#lastNameErr").addClass("text-hide");
            $("#lastNameGroup").removeClass("has-error");
            formState.lastName = true;

            //Validate the whole form
            if (checkTrueness(formState))
                $("#submitUser").removeAttr("disabled");
        }
    }
    $("#lastName").keyup(function (e) {
        checkLastName($(this).val(), e);
    }).bind("paste", function (e) { //When pasting from context menu, you need a small delay to capture the text
        setTimeout(function () {
            checkLastName($("#lastName").val(), e);
        }, 10);
    });

    //For username, we want to alert the user if they have >5 chars, or an empty field
    function checkUsername(val, e) {
        var len = val.length;
        if (len > 5 || len <= 0) { //Incorrect field
            $("#usernameErr").removeClass("text-hide");
            $("#usernameGroup").addClass("has-error");
            formState.username = false;
            $("#submitUser").prop("disabled", true);
        } else {
            $("#usernameErr").addClass("text-hide");
            $("#usernameGroup").removeClass("has-error");
            formState.username = true;

            //Validate the whole form
            if (checkTrueness(formState))
                $("#submitUser").removeAttr("disabled");
        }
    }
    $("#username").keyup(function (e) {
        checkUsername($(this).val(), e);
    }).bind("paste", function (e) {
        setTimeout(function () { //When pasting from context menu, you need a small delay to capture the text
            checkUsername($("#username").val(), e);
        }, 10);
    });

    //Check whether the two password fields are a match
    function checkPassword(passVal, passConfVal , e) {
        var match = passConfVal.toString() == passVal.toString();
        if (!match || (!passConfVal || !passVal)) { //Incorrect field
            $("#passwordErr").removeClass("text-hide");
            $("#passwordGroup").addClass("has-error");
            $("#passwordConfirmErr").removeClass("text-hide");
            $("#passwordConfirmGroup").addClass("has-error");
            formState.passwordMatch = false;
            $("#submitUser").prop("disabled", true);
        } else {
            $("#passwordErr").addClass("text-hide");
            $("#passwordGroup").removeClass("has-error");
            $("#passwordConfirmErr").addClass("text-hide");
            $("#passwordConfirmGroup").removeClass("has-error");
            formState.passwordMatch = true;

            //Validate the whole form
            if (checkTrueness(formState))
                $("#submitUser").removeAttr("disabled");
        }
    }

    //For password, we want to alert the user if they haven't got matching passwords
    $("#password").keyup(function (e) {
        checkPassword($(this).val(), $("#passwordConfirm").val(), e);
    }).bind("paste", function (e) {
        setTimeout(function () { //When pasting from context menu, you need a small delay to capture the text
            checkPassword($("#password").val(), $("#passwordConfirm").val(), e);
        }, 10);
    });

    //For passwordConfirm, we want to alert the user if they haven't got matching passwords
    $("#passwordConfirm").keyup(function (e) {
        checkPassword($("#password").val(), $(this).val(), e);
    }).bind("paste", function (e) {
        setTimeout(function () { //When pasting from context menu, you need a small delay to capture the text
            checkPassword($("#password").val(), $("#passwordConfirm").val(), e);
        }, 10);
    });

    //When the form is submitted, construct the User object and post it to the endpoint
    $("#submitUser").click(function (e) {
        e.preventDefault();

        var user = {
            Id : 0,
            FirstName : $("#firstName").val(),
            LastName : $("#lastName").val(),
            Username : $("#username").val(),
            EmailAddress : $("#emailAddress").val()
        };
        addUser(user);
    });
});