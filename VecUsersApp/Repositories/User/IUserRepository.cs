﻿using System.Collections.Generic;
using VecUsersApp.Models.User;

namespace VecUsersApp.Repositories.User
{
    public interface IUserRepository
    {
        void AddUser(UserModel user);
        bool RemoveUser(int userId);
        List<UserModel> GetUsers();
    }
}
