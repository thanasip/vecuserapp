﻿using System.Collections.Generic;
using VecUsersApp.Models.User;

namespace VecUsersApp.Repositories.User
{
    public class UserRepository : IUserRepository
    {
        //This static list will act as our "database"
        private static List<UserModel> _users;

        //This will keep track of Ids as an IDENTITY(1,1) column PK in a database might
        private static int _id = 2;

        //Initialize our "database" with some data
        static UserRepository()
        {
            _users = new List<UserModel>
            {
                new UserModel
                {
                    Id = 0,
                    FirstName = "Ken",
                    LastName = "Thompson",
                    Username = "ken",
                    EmailAddress = "ken@example.com"
                },
                new UserModel
                {
                    Id = 1,
                    FirstName = "Brian",
                    LastName = "Kernighan",
                    Username = "bwk",
                    EmailAddress = "bwk@example.com"
                },
                new UserModel
                {
                    Id = 2,
                    FirstName = "Dennis",
                    LastName = "Ritchie",
                    Username = "dmr",
                    EmailAddress = "dmr@example.com"
                }
            };
        }

        public void AddUser(UserModel user)
        {
            _id++; //Increment our counter
            user.Id = _id;
            _users.Add(user);
        }

        public List<UserModel> GetUsers()
        {
            return _users;
        }

        public bool RemoveUser(int userId)
        {
            int index = _users.FindIndex(user => user.Id == userId);
            try
            {
                _users.RemoveAt(index);
            }
            catch //No need to capture the specific exception because there's nothing the user can do anyway
            {
                return false; //We couldn't remove that user
            }
            return true; //We removed that user
        }
    }
}
