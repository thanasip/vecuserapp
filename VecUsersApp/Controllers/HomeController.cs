﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using VecUsersApp.Models;

namespace VecUsersApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "This application was developed for the VEC coding assignment.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "My information:";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
