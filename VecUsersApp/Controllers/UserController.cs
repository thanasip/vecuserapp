﻿using Microsoft.AspNetCore.Mvc;
using VecUsersApp.Models.User;
using VecUsersApp.Repositories.User;

namespace VecUsersApp.Controllers
{
    public class UserController : Controller
    {
        private UserRepository _repo;

        //Inject the repository interface as a dependency
        public UserController(IUserRepository repo)
        {
            _repo = (UserRepository) repo;
        }

        [Route("users")]
        public IActionResult GetUsers()
        {
            return Json(_repo.GetUsers());
        }

        [Route("user/new"), HttpPost]
        public IActionResult AddUser(UserModel user)
        {
            _repo.AddUser(user);
            return Ok();
        }

        [Route("user/delete"), HttpPost]
        public IActionResult RemoveUser(int userId)
        {
            return
            (
                _repo.RemoveUser(userId)
                ? (IActionResult) Ok()
                : (IActionResult) BadRequest()
            );
        }
    }
}